CC=gcc
exports = spl.y makefile instr.h instr.c interp.h interp.c spl.h splmain.h splmain.c st.h st.c lexer.h lexer.c

spl:	splmain.o parser.o lexer.o st.o instr.o interp.o
	gcc $(CFLAGS) -o spl splmain.o parser.o lexer.o st.o instr.o interp.o
lexdef.h parser.c:	spl.y
	yacc -vdy spl.y
	mv y.tab.h lexdef.h
	mv y.tab.c parser.c
splmain.o:	splmain.c spl.h interp.h lexdef.h
lexer.o:	lexer.c lexdef.h spl.h
parser.o:	parser.c spl.h st.h instr.h
st.o:	st.c st.h
instr.o:	instr.c instr.h
interp.o:	interp.c interp.h
export:
	tar cfz spl.tgz $(exports)
	zip spl.zip $(exports)
clean:
	rm -f spl spl.zip spl.tgz *.o y.output parser.c lexdef.h
print:
	a2ps -1 --portrait --font-size=9 -o spl.ps $(exports) 
