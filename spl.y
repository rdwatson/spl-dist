%{
#include <stdio.h>
#include <stdlib.h>
#include "spl.h"
#include "st.h"
#include "instr.h"

struct symtab *globalst,*localst,*stp;
struct st_entry *ep;
struct attr at;
int idclass,present,codestart,startpc,pc,varlocn,himem,numlocal,local;
char emsg[80];
int actmsg;
int freereg[FP],pcount,reg;
long mem[MEMSIZE];
%}
%union{
    int number;     /* value of NUM token               */
    char id[MAXIDLEN+1];    /* value if IDENT token     */
    int temp;       /* temp register                    */
    int symval;     /* value of symbol/operator token   */
}
%token  VAR 1
%token  PROC    2
%token  IF  3
%token  THEN    4
%token  ELSE    5
%token  WHILE   6
%token  DO  7
%token  ACCEPT  8
%token  DISPLAY 9
%token  <symval> OR 10
%token  <symval> AND    11
%token  <symval> MOD    12
%token  NOT 13

%token  SEMICOLON   101
%token  COMMA   102
%token  LBRACE  103
%token  RBRACE  104
%token  LPAREN  105
%token  RPAREN  106
%token  <symval> PLUS   107
%token  <symval> MINUS  108
%token  <symval> BY 109
%token  <symval> DIVIDE 110
%token  <symval> EQ 111
%token  <symval> LT 112
%token  <symval> GT 113
%token  <symval> LEQ    114
%token  <symval> GEQ    115
%token  <symval> NEQ    116
%token  <symval> ASSIGN 117

%token  <id>    IDENT   201
%token  <number>    NUM 202
%token  LEXERR  203
%token  LEXEOF  0   

%type <temp>    factor term expression statement stmtlist else_part
%type <temp>    logical_exp condition
%type <symval>  mulop addop relop logop
%%
program :   { stp=globalst=create_st(); local=0; freeallregs();}
        vardecl
        { codestart = pc = varlocn; local=1;}
        procdecllist
        { init_main();}
        c_statement
        {if (actmsg) list_st(globalst);
        mem[pc++]=encode(SVC,DIRECT,0,0,0);
        /* listcode(codestart,pc-1); */
        }
    ;
vardecl :   VAR
        { if (local)    {idclass=VAR_CLASS; varlocn = -1;}
          else      {idclass=GLOB_VAR_CLASS; varlocn = 16;}
        }
        identlist SEMICOLON
    |
    ;
procdecllist:   procdecllist procdecl
    |
    ;
identlist:  identlist COMMA IDENT
        { enter_symbol(stp,$3,idclass); }
    |   IDENT
        { enter_symbol(stp,$1,idclass); }
    ;
procdecl:   PROC
        IDENT
        { procinit();}
        params
        { numlocal=0;}
        vardecl 
        {   if (actmsg) list_st(stp); 
            enter_symbol(globalst,$2,PROC_CLASS); 
            proc_begin($2); 
        }
        c_statement
        { proc_end(); }
    ;
params:     LPAREN { idclass=PARAM_CLASS; } identlist RPAREN
    |
    ;
statement:  c_statement {$$=0;}
    |   IDENT ASSIGN expression SEMICOLON
{   if(lookup($1,&ep)){
        get_attr(ep,&at);
        switch(at.id_class){
        case GLOB_VAR_CLASS:
            mem[pc++]=encode(MOVM,DIRECT,$3,0,at.id_locn); break;
        case VAR_CLASS:
            mem[pc++]=encode(MOVM,INDEXED,$3,FP,at.id_locn); break;
        case PARAM_CLASS:
            mem[pc++]=encode(MOVM,INDIRIX,$3,FP,at.id_locn); break;
        }
        free1reg($3); $$=0;
    }
    else{
        sprintf(emsg,"undeclared identifier %s",$1);
        yyerror(emsg);
    }   
}
    |   IDENT {pcount=0;} actual_params SEMICOLON
{   if(lookup($1,&ep)){
        get_attr(ep,&at);
        if (at.id_class==PROC_CLASS){
        reg=getreg();
            mem[pc++]=encode(MOVI,DIRECT,reg,0,pcount);
            mem[pc++]=encode(PUSH,DIRECT,SP,0,reg);
            mem[pc++]=encode(JSP,DIRECT,SP,0,at.id_locn);
            free1reg(reg);
        }
        else{
        sprintf(emsg,"identifier %s is not a procedure",$1);
        yyerror(emsg);
        }
    }
    else{
        sprintf(emsg,"undeclared procedure identifier %s",$1);
        yyerror(emsg);
    }
}
    |   IF condition 
        { mem[pc]=encode(JEQ,DIRECT,$2,0,0);push(pc++);}
        THEN statement else_part {$$=0;}
    |   WHILE {push(pc);}
        condition DO { mem[pc]=encode(JEQ,DIRECT,$3,0,0); push(pc++);}
        statement
{       backpatch(pop(),pc+1);  
        mem[pc++]=encode(JMP,DIRECT,0,0,pop());
        $$=0;
}
    |   ACCEPT IDENT SEMICOLON      { io($2,IN); $$=0;}
    |   DISPLAY displist SEMICOLON      {$$=0;}
    ;
else_part:  /* null */
        { backpatch(pop(),pc); $$=0;}
    |
        ELSE
{       mem[pc++]=encode(JMP,DIRECT,0,0,0);
        backpatch(pop(),pc);
        push(pc-1);
        $<temp>$=0;
}
        statement   {backpatch(pop(),pc); $$=0;}
    ;
displist:   displist COMMA IDENT    {io($3,OUT);}
    |   IDENT           {io($1,OUT);}
    ;
actual_params:  LPAREN paramlist RPAREN 
    |   LPAREN RPAREN
    ;
paramlist:  IDENT COMMA paramlist   { push_param($1); pcount++;}
    |   IDENT           { push_param($1); pcount++;}
    ;
c_statement:    LBRACE stmtlist RBRACE
    ;
stmtlist:   stmtlist statement
    |   statement
    ;
expression: expression addop term
{   switch($2){
        case PLUS:  mem[pc++]=encode(IADD,DIRECT,$1,0,$3);  break;
        case MINUS: mem[pc++]=encode(ISUB,DIRECT,$1,0,$3);  break;
        case MOD:   break;
    }
    free1reg ($3);
    $$=$1;
}
    |   term    {$$=$1;}
    ;
term:       term mulop factor
{   switch($2){
        case BY:    mem[pc++]=encode(IMUL,DIRECT,$1,0,$3);  break;
        case DIVIDE:mem[pc++]=encode(IDIV,DIRECT,$1,0,$3);  break;
        case MOD:   break;
    }
    free1reg ($3);
    $$=$1;
}
    |   factor  {$$=$1;}
    ;
factor:     IDENT
{   
    if (lookup($1,&ep)){
        reg=getreg();
        get_attr(ep,&at);
        switch (at.id_class){
        case GLOB_VAR_CLASS:
            mem[pc++]=encode(MOV,DIRECT,reg,0,at.id_locn); break;
        case VAR_CLASS:
            mem[pc++]=encode(MOV,INDEXED,reg,FP,at.id_locn); break;
        case PARAM_CLASS:
            mem[pc++]=encode(MOV,INDIRIX,reg,FP,at.id_locn); break;
        }
    $$=reg;
    }
    else{
        sprintf(emsg,"undeclared identifier %s",$1);
        yyerror(emsg);
    }   
}
    |   NUM
{
    reg=getreg();
    mem[pc++]=encode(MOVI,DIRECT,reg,0,$1);
    $$=reg;
}
    |   LPAREN expression RPAREN{$$=$2;} 
    |   MINUS expression
{   reg=getreg();
    mem[pc++]=encode(MOVI,DIRECT,reg,0,0);
    mem[pc++]=encode(ISUB,DIRECT,reg,0,$2);
    free1reg($2);
    $$=reg;
}
    ;
addop:      PLUS
    |   MINUS
    ;
mulop:      BY
    |   DIVIDE
    |   MOD
    ;
condition:  condition logop logical_exp
{   int logopcode;
    switch ($2){
        case AND:   logopcode=LAND; break;
        case OR:    logopcode=LOR;  break;
    }
    mem[pc++]=encode(logopcode,DIRECT,$1,0,$3);
    free1reg($2);
    $$=$1;
}
    |   logical_exp {$$=$1;}
    ;
logical_exp:    expression relop expression
{   int jmpop;
    reg=getreg();
    switch($2){
        case EQ:    jmpop=JEQ;  break;
        case NEQ:   jmpop=JNE;  break;
        case GT:    jmpop=JGT;  break;
        case LT:    jmpop=JLT;  break;
        case GEQ:   jmpop=JGE;  break;
        case LEQ:   jmpop=JLE;  break;
    }
    mem[pc++]=encode(MOVI,DIRECT,reg,0,1);  /* reg = TRUE   */
    mem[pc++]=encode(ISUB,DIRECT,$1,0,$3);  /* compare  */
    mem[pc]=encode(jmpop,DIRECT,$1,0,pc+2); pc++;
    mem[pc++]=encode(MOVI,DIRECT,reg,0,0);  /* reg = FALSE  */
    free1reg ($1); free1reg ($2);
    $$=reg;
}
    |   NOT logical_exp
{   mem[pc++]=encode(MOVI,DIRECT,reg,0,1);
    mem[pc++]=encode(ISUB,DIRECT,reg,0,$2);
    free1reg ($2);
    $$=reg; 
}
    ;
relop:      EQ
    |   NEQ
    |   GT
    |   LT
    |   GEQ
    |   LEQ
    ;
logop:      AND
    |   OR
    ;
%%
init_main(){
    startpc=pc;
    mem[pc++]=encode(MOVI,DIRECT,SP,0,MEMSIZE);
    mem[pc++]=encode(MOVI,DIRECT,FP,0,0);
}
procinit(){
    if (localst!=NULL) destroy_st(localst); localst=NULL;
    stp=localst=create_st();
    varlocn=3;
}

enter_symbol(stp,id,idclass) struct symtab *stp; char *id; int idclass;{
    struct st_entry *ep;
    int present;
    struct attr at;

    enter(stp,id,&ep,&present);
    if (present){
        sprintf(emsg,"identifier %s already declared",id);
        yyerror(emsg);
    }
    else{
        at.id_class=idclass;
        switch (idclass) {
            case VAR_CLASS:     numlocal++;
                        at.id_locn=varlocn--; break;
            case GLOB_VAR_CLASS:
            case PARAM_CLASS:   at.id_locn=varlocn++;   break;

            case PROC_CLASS:    at.id_locn=pc;
                        at.num_locals=numlocal; break;
        }
        set_attr(ep,&at);
    }
}

/* lookup an identifier
 * return true if found
 */
int lookup(s,ep) char *s; struct st_entry **ep;{
    int found;

    find (localst,s,ep,&found);
    if (!found) find (globalst,s,ep,&found);
    return (found);
}
/*  stack used for backpatching jump addresses  */
#define MAXSP 16
int stack[MAXSP];
int sp = -1;
push(v) int v;{
    if (sp<MAXSP-1) stack[++sp]=v;
    else{
        sprintf(emsg,"compiler error: stack overflow");
        yyerror(emsg);
        exit(1);
    }
}
int pop() {
    if (sp>=0) return( stack[sp--]);
    else
        sprintf(emsg,"compiler error: stack underflow");
        yyerror(emsg);
        exit(1);
    }
backpatch(instr_addr, jump_dest) int instr_addr, jump_dest;{
    mem[instr_addr] |= jump_dest;
}
    
int getreg(){   /* get a register (temporary)   */
    int i;

    for (i=0; i<FP; i++)
        if (freereg[i]){
            freereg[i]=0;   /* allocate it  */
            return(i);
        }
    /* here if no free regs */
    sprintf(emsg,"compiler error: can't allocate register");
    yyerror(emsg);
    exit(1);
}

free1reg(r) int r;{ /* deallocate a temporary register  */
    freereg[r]=1;
}

freeallregs(){  /* deallocate all temporary registers   */
    int i;

    for (i=0; i<FP; i++) freereg[i]=1;
}
io(id,io_op) char *id; int io_op;{ /* input or output operation */
/* note: these 2 operations (IN OUT) do NOT use a target register   */
    if(lookup(id,&ep)){
        get_attr(ep,&at);
        switch(at.id_class){
        case GLOB_VAR_CLASS:
            mem[pc++]=encode(io_op,DIRECT,0,0,at.id_locn); break;
        case VAR_CLASS:
            mem[pc++]=encode(io_op,INDEXED,0,FP,at.id_locn); break;
        case PARAM_CLASS:
            mem[pc++]=encode(io_op,INDIRIX,0,FP,at.id_locn); break;
        }
    }
}
push_param(s) char *s;{

    if(lookup(s,&ep)){
        get_attr(ep,&at);
        reg=getreg();
        /* now place address of param in reg    */
        switch(at.id_class){
        case GLOB_VAR_CLASS:
            mem[pc++]=encode(MOVI,DIRECT,reg,0,at.id_locn); break;
        case VAR_CLASS:
            mem[pc++]=encode(MOVI,INDEXED,reg,FP,at.id_locn); break;
        case PARAM_CLASS:
            mem[pc++]=encode(MOV,INDEXED,reg,FP,at.id_locn); break;
        }
        mem[pc++]=encode(PUSH,DIRECT,SP,0,reg);
        free1reg(reg);
    }
    else{
        sprintf(emsg,"undeclared identifier %s",s);
        yyerror(emsg);
    }   
}
proc_begin(char *name){
    if (actmsg) printf("*** proc_begin(%s) ***\n",name);
    mem[pc++]=encode(PUSH,DIRECT,SP,0,FP);
    mem[pc++]=encode(MOV,DIRECT,FP,0,SP);
/*  mem[pc++]=encode(ISUB,INDEXED,SP,FP,2); dec sp by arg count */
    reg=getreg();
    mem[pc++]=encode(MOVI,DIRECT,reg,0,numlocal);
    mem[pc++]=encode(ISUB,DIRECT,SP,0,reg); /* dec sp by #local var */
}
proc_end() {
    if (actmsg) printf("*** proc_end ***\n");
    reg=getreg();
    mem[pc++]=encode(MOVI,DIRECT,reg,0,3);
    mem[pc++]=encode(MOV,DIRECT,SP,0,FP);   /* sp <- fp     */
    mem[pc++]=encode(IADD,DIRECT,SP,0,reg); /* sp <- fp+3       */
    mem[pc++]=encode(IADD,INDEXED,SP,FP,2); /* sp <- fp+3+argcount  */
    mem[pc++]=encode(PUSH,INDEXED,SP,FP,1); /* push the r.a.    */
    mem[pc++]=encode(MOV,INDEXED,FP,FP,0);  /* reset the fp     */
    mem[pc++]=encode(RET,0,SP,0,0);
    free1reg(reg);
    destroy_st(localst); localst=NULL;
}

listcode(start,finish) int start,finish;{
    int i,op,mode,target,index,addr;
    for (i=start; i<=finish; i++){
        printf("%5d:",i);
        decode(mem[i],&op,&mode,&target,&index,&addr);
        dispinst(stdout,op,mode,target,index,addr);
        printf("\n");
    }
}
