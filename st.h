/* st.h	
 * declarations for users of symbol table routines - SIMPLE version
 */

/* data structures	*/

#ifndef MAXIDLEN
# include "spl.h"
#endif

#define MAXSTSIZE	100

struct attr{		/* identifier attributes	*/
	int id_class;
	int id_locn;
	int num_locals; /* for procedures only	*/
};

struct st_entry{	/* symbol table entry		*/
	char	name[MAXIDLEN+1];
	struct attr 	id_attr;
};

struct symtab{		/* THE symbol table		*/
	struct st_entry ste[MAXSTSIZE];
};

/* values of id_class	*/
#define GLOB_VAR_CLASS	0
#define	VAR_CLASS        1
#define	PROC_CLASS       2
#define	PARAM_CLASS      3

/* exernal definitions	*/
struct symtab *create_st();

void destroy_st(struct symtab *st);
void enter(struct symtab *st, char *name_str, struct st_entry **entry, int *present);
void find(struct symtab *st, char *name_str, struct st_entry **entry, int *present);
void set_attr(struct st_entry *entry, struct attr *entry_attr);
void get_attr(struct st_entry *entry, struct attr *entry_attr);
void list_st(struct symtab *st);
