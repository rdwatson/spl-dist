#define MEMSIZE	200
#define MAXIDLEN 12
#define YYDEBUG 1

/* used by lexer    */ 
extern FILE *infile;

/* these are defined in spl.y   */
extern int yydebug, actmsg;     
extern int pc, startpc, codestart ;
extern long mem[];
