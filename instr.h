/*      instr.h */
/*      define opcodes and other codes for SVM  */
/*      pseudo-ops      */
#define RES     0       /* reserve storage                      */
#define BLK     1       /* a block of 0 filled bytes            */
#define START   2       /* start address                        */
#define LIMIT   3       /* max number of machine instructions   */
#define MAX_NUM_PSEUDO  3
/*      true opcodes    */
#define NOP     4
#define MOV     5
#define MOVM    6
#define PUSH    7
#define POP     8
#define IADD    9
#define ISUB    10
#define IMUL    11
#define IDIV    12
#define LAND    13
#define LOR     14
#define XOR     15
#define JMP     16
#define JLT     17
#define JLE     18
#define JGT     19
#define JGE     20
#define JEQ     21
#define JNE     22
#define JSP     23
#define RET     24
#define IN      25
#define OUT     26
#define SVC     27
#define MOVI    28
/*      change this value if new instructions added     */
#define NUM_INSTR 29

/*      addressing modes        */
#define DIRECT          0
#define INDEXED         1
#define INDIRECT        2
#define INDIRIX         3

#define SP      15      /* stack pointer        */
#define FP      14      /* frame pointer        */

long encode(int opcode,int mode,int target,int index,int addr);
void decode(long,int *opcode,int *mode,int *target,int *index,int *addr);
void dispinst(FILE *fp,int opcode,int mode,int target,int index,int addr);

