#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "spl.h"
#include "interp.h"
#include "lexdef.h" 
#include "lexer.h" 

FILE *infile;
int action;
int idebug;     /* interpreter debugging    */
int compileError = 0;

#define LEX     0
#define PARSE   1
#define INTERP  2

int main(argc,argv) int argc; char *argv[];{
	int i;
    int opt;
    int tok;
    int res;
    
    /* defaults */
	yydebug = 0;
	infile	= stdin;
    action  = INTERP;
    idebug  = 0;  /* dont trace in interp */
    actmsg  = 0;  /* don't show parser action messages  */

    while ((opt = getopt(argc, argv, "dvphl")) != -1) {
        switch (opt) {
        case 'd':   idebug = SSTEP; 
                    break;
        case 'l':   action = LEX; 
                    break;
        case 'v':   yydebug=1;
                    break;
        case 'p':   action = PARSE;
                    break;
        case 'h':   
        default:
                    fprintf(stderr, "Usage: %s [-dvhl] [file]\n"
                                    "  -d  run interpreter in debug mode\n"
                                    "  -h  print this message\n"
                                    "  -l  run the lexer only\n"
                                    "  -p  run the parser only\n"
                                    "  -v  verbose output from parser\n"
                                    , argv[0]);
                    exit(opt!='h');
        }
    }

    if (argc>optind) {
        /* more args on cmd line, assume file     */
		if ((infile=fopen(argv[optind],"r"))==NULL){
			fprintf(stderr,"spl: can't open %s\n",argv[optind]);
			exit(1);
		}
    }

	if (action==PARSE) {
        res = yyparse();
        if (res != 0){
            printf("error near line %d, column %d\n",lineno,colno);
            return 1;
        } else {
            if (!compileError) listcode(codestart,pc-1);
            return 0;
        }
    } else if (action==INTERP) {
        if (yyparse()==0) {
            /* successful parse but maybe semantic errors*/
            if (!compileError) {
                listcode(codestart,pc-1);
                interp(mem,startpc,5000,MEMSIZE,pc,idebug);
            }
        } else {
            printf("error near line %d, column %d\n",lineno,colno);
            return 1;
        }
    }
    else if (action==LEX) { 
        while ((tok=yylex()) != LEXEOF) dispToken(tok);
    }

        
}

yyerror(s) char *s;{
	fprintf(stderr,"line %d col %d: %s\n",lineno,colno,s);
    compileError=1;
}
