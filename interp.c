/* interp.c
 *  the interpreter for the Simple Virtual Machine
 *  parameters:
 *      mem:    pointer to memory store
 *      startpc:first operation to be executed
 *      timelim:max. number of instructions that will be executed
 *      memsize:size of mem[]
 *      stklim: minimum value of stack pointer(initially == memsize)
 *      debug:  debug flag
 */

#include <ctype.h>
#include <stdio.h>
#include "interp.h"
#include "instr.h"
static int pc;
static long pop(); static push();

void interp(long mem[], int startpc,int timelim,
            int memsize,int stklim, int debug){

    int ea;     /* effective address    */
    int icount; /* count instructions   */
    int exit_flag=0;
    int opcode,mode,target,index,addr;
    char cmd[80];

    pc=startpc;
    for(icount=0; icount<timelim && !exit_flag; icount++){
        decode(mem[pc++],&opcode,&mode,&target,&index,&addr);
        /* calculate effective address  */
        switch (mode){
            case DIRECT:    ea=addr;                break;
            case INDEXED:   ea=addr + mem[index];   break;
            case INDIRECT:  ea=mem[addr];           break;
            case INDIRIX:   ea=mem[addr+mem[index]];break;
        }
        if (debug){
            printf("pc:%5d ",pc-1);
            dispinst(stdout,opcode,mode,target,index,addr);
            printf("\tEA:%5d\n",ea);
        }

        if (debug==SSTEP)
            while(1){
            int r,n;
            printf("debug> "); scanf("%s",cmd);
            if (toupper(*cmd)=='R')
                for (r=0; r<16; r++){
                     printf("%-2d %5d ",r,mem[r]);
                     if(r%8==7) printf("\n");
                }
            else if (isdigit(*cmd)){
                sscanf(cmd,"%d",&n);
                printf("%5d: %8ld\n",n,mem[n]);
            }
            else if (toupper(*cmd)=='G') {debug=OFF; break;}
            else if (toupper(*cmd)=='T') {debug=TRACE; break;}
            else if (toupper(*cmd)=='S') {break;}
            }

        if (ea>memsize || ea<0)
            if (opcode != MOVI){
                ierr(pc-1,"illegal memory reference");
                break;
            }
        switch (opcode){
            case NOP:   break;
            case MOV:   mem[target]=mem[ea];                break;
            case MOVM:  mem[ea]=mem[target];                break;
            case MOVI:  mem[target]=ea;                     break;
            case PUSH:  push(mem,target,mem[ea],stklim);    break;
            case POP:   mem[ea]=pop(mem,target,memsize);    break;
            case IADD:  mem[target]+=   mem[ea];            break;
            case ISUB:  mem[target]-=   mem[ea];            break;
            case IMUL:  mem[target]*=   mem[ea];            break;
            case IDIV:      mem[target]/=   mem[ea];        break;
            case LAND:  mem[target]=mem[target] && mem[ea]; break;
            case LOR:   mem[target]=mem[target] || mem[ea]; break;
            case XOR:   mem[target]^=   mem[ea];            break;
            case JMP:   pc=ea;                              break;
            case JLT:   if (mem[target]<0)  pc=ea;          break;
            case JLE:   if (mem[target]<=0) pc=ea;          break;
            case JGT:   if (mem[target]>0)  pc=ea;          break;
            case JGE:   if (mem[target]>=0) pc=ea;          break;
            case JEQ:   if (mem[target]==0) pc=ea;          break;
            case JNE:   if (mem[target]!=0) pc=ea;          break;
            case JSP:   push(mem,target,pc,stklim); pc=ea;  break;
            case RET:   pc=pop(mem,target,memsize);         break;
            case IN:    putchar('?'); scanf("%d",&mem[ea]); break;
            case OUT:   printf("%d\n",mem[ea]);             break;
            case SVC:   exit_flag=1; printf("-=end of program=-\n"); break;
            default:    exit_flag=1; ierr(pc-1,"illegal opcode"); break;
        }
    }
    printf("program terminated: time= %d instructions\n",icount);
    if (icount>=timelim)
        printf("abnormal termination: time limit exceeded\n");
    else
        printf("normal termination\n");
}
/* mem[sp] always points to TOP OF STACK
 * stack pointer grows by DECREMENTING
 */
static push(mem,sp,v,stklim) long mem[]; int sp; long v; int stklim;{
    if (mem[sp]>stklim)
        mem[--mem[sp]]=v;
    else
        ierr(pc-1,"stack overflow");
}

static long pop(mem,sp,memsize) long mem[]; int sp; int memsize; {
    if (mem[sp]<memsize)
        return(mem[mem[sp]++]);
    else
        ierr(pc-1,"stack underflow");
}

ierr(pc,s) int pc; char *s;{
    fprintf(stderr,"interpreter error at %5d: %s\n",pc,s);
}
