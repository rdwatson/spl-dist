#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "spl.h"
#include "lexdef.h"
#include "lexer.h"

#define MAXNUMLEN   8
#define NUMLENERR   1

struct rw{
    char    *resword;
    int restokval;
};
struct rw rwtable[]={
    "accept",   ACCEPT,
    "and",      AND,
    "display",      DISPLAY,
    "do",       DO,
    "else",     ELSE,
    "if",       IF,
    "mod",      MOD,
    "not",      NOT,
    "or",       OR,
    "procedure",    PROC,
    "then",     THEN,
    "var",      VAR,
    "while",    WHILE
};

int norw = sizeof(rwtable)/sizeof(struct rw);   /* number of reserved words */
int iscomment(char);

int lineno = 1;     /* line num of token                */
int colno  = 0;     /* col num of start of token        */

static int zzlex();

#ifdef YYDEBUG
extern int yydebug;
#endif
extern YYSTYPE yylval;

void dispToken(int token){
        printf ("line %d: ",lineno);
        switch (token){
        case VAR:   printf("VAR\n");    break;
        case PROC:  printf("PROC\n");   break;
        case IF:    printf("IF\n"); break;
        case THEN:  printf("THEN\n");   break;
        case ELSE:  printf("ELSE\n");   break;
        case WHILE: printf("WHILE\n");  break;
        case DO:    printf("DO\n"); break;
        case ACCEPT:    printf("ACCEPT\n"); break;
        case DISPLAY:   printf("DISPLAY\n");    break;
        case OR:    printf("OR\n"); break;
        case AND:   printf("AND\n");    break;
        case MOD:   printf("MOD\n");    break;
        case NOT:   printf("NOT\n");    break;

        case SEMICOLON: printf("SEMICOLON\n");  break;
        case COMMA: printf("COMMA\n");  break;
        case LBRACE:    printf("LBRACE\n"); break;
        case RBRACE:    printf("RBRACE\n"); break;
        case LPAREN:    printf("LPAREN\n"); break;
        case RPAREN:    printf("RPAREN\n"); break;
        case PLUS:  printf("PLUS\n");   break;
        case MINUS: printf("MINUS\n");  break;
        case BY:    printf("BY\n"); break;
        case DIVIDE:    printf("DIVIDE\n"); break;
        case EQ:    printf("EQ\n"); break;
        case LT:    printf("LT\n"); break;
        case GT:    printf("GT\n"); break;
        case LEQ:   printf("LEQ\n");    break;
        case GEQ:   printf("GEQ\n");    break;
        case NEQ:   printf("NEQ\n");    break;
        case ASSIGN:    printf("ASSIGN\n"); break;

        case IDENT: printf("IDENT: %s\n",yylval.id);break;
        case NUM:   printf("NUM: %9d\n",yylval.number);break;
        case LEXERR:    printf("LEXERR\n"); break;
        case LEXEOF:    printf("LEXEOF\n"); break;
        default: printf("default token=%4d\n",token);
        }
}

yylex(){
    int token;
    
    token=zzlex();
#ifdef YYDEBUG
    if (yydebug) {
        printf(">>\t\t\t\t%4o  %3d  ",token,token);
        dispToken(token);
    }
#endif
    return(token);
}

int lexgetc(FILE *fp){
    colno++;
    return getc(fp);
}
int lexungetc(int c, FILE *fp){
    colno--;
    return ungetc(c,fp);
}

int zzlex(){
    int c;
    char id[MAXIDLEN+1];
    int count;
    int i,j,k,comp;
    int val;

    c=lexgetc(infile);
    while(isspace(c) || iscomment(c))
    /* skip white space OR comment  */
        if (isspace(c)) { 
            if (c=='\n') {lineno++; colno=0;}
            c=lexgetc(infile);
        }
        else while((c=lexgetc(infile))!='\n') /* gobble till eoln */ ; 
    if (isalpha(c)){
        /* reserved word of identifier */
        count=0;
        do{
            if (count<MAXIDLEN) id[count++]=c;
            c=lexgetc(infile);
        } while (isalpha(c) | isdigit(c) | c=='_');
        lexungetc(c,infile);
        id[count]='\0';
        /* search reserved word table   */
        i=0;    j=norw-1;
        do{
            k=(i+j)/2;
            comp=strcmp(id,rwtable[k].resword);
            if (comp<0) j=k-1;
            else if (comp>0) i=k+1;
        } while (comp!=0 & i<=j);
        if (comp==0)
            return(yylval.symval = rwtable[k].restokval);
        else{
            strcpy(yylval.id,id);
            return(IDENT);
        }
    }
    else if (isdigit(c)){
        /* NUM  */
        val=0; count=0;
        do{
            val=10*val + c - '0';
            count++; c=lexgetc(infile);
        }while (isdigit(c));
        lexungetc(c,infile);
        if (count>=MAXNUMLEN || val>32767){
            yylval.number=0;
            return(LEXERR);
        }
        else{
            yylval.number=val;
            return (NUM);
        }
    }
    /* symbols  */
    else if (c=='>')
        if ((c=lexgetc(infile))=='=')
            return (yylval.symval=GEQ);
        else{
            lexungetc(c,infile);
            return (yylval.symval=GT);
        }
    else if (c=='<')
        if ((c=lexgetc(infile))=='=')  return (yylval.symval=LEQ);
        else if (c=='>')        return (yylval.symval=NEQ);
        else{
            lexungetc(c,infile);
            return (yylval.symval=LT);
        }
    else if (c==':')
        if ((c=lexgetc(infile))=='=')
            return (yylval.symval=ASSIGN);
        else{
            lexungetc(c,infile);
            return (LEXERR);
        }
    else if (c==';')    return (yylval.symval=SEMICOLON);
    else if (c==',')    return (yylval.symval=COMMA);
    else if (c=='{')    return (yylval.symval=LBRACE);
    else if (c=='}')    return (yylval.symval=RBRACE);
    else if (c=='(')    return (yylval.symval=LPAREN);
    else if (c==')')    return (yylval.symval=RPAREN);
    else if (c=='+')    return (yylval.symval=PLUS);
    else if (c=='-')    return (yylval.symval=MINUS);
    else if (c=='*')    return (yylval.symval=BY);
    else if (c=='/')    return (yylval.symval=DIVIDE);
    else if (c=='=')    return (yylval.symval=EQ);
    else if (c==EOF)    return (LEXEOF);
    else
        return (yylval.symval=LEXERR);
}

int iscomment(char c){
    if (c=='-') 
        if ((c=lexgetc(infile))=='-')
            return 1;
        else{
            lexungetc(c,infile);
            return 0;
        }
    else 
        return 0;
}

