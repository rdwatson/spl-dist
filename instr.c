/* codes.c - instruction word encode/decode
 *	format:(opcode is MSBits)
 *		opcode:	6 bits
 *		mode:	2 bits; 1st bit=INDIRECT; 2nd bit=INDEX
 *		target,index registers: 4 bits each
 *		address: 16 bits
 */
#include <stdio.h>
#include <string.h>
#include "instr.h"
union instruction{
	struct{ short int address, first_part;} i1;
	long i2;
};
/*	code tables-
 CODES MUST BE IN ORDER DEFINED BY CONSTANTS IN  instr.h
*/
static char *optable[]={"RES","BLK","START","LIMIT",
		"NOP","MOV","MOVM","PUSH","POP","IADD","ISUB","IMUL","IDIV",
		"LAND","LOR","XOR",
		"JMP","JLT","JLE","JGT","JGE","JEQ","JNE","JSP","RET",
		"IN","OUT","SVC","MOVI"};
#define NUMOP	(sizeof(optable)/sizeof(char *))
/*	mode table	*/
static char *modtable[]={"DIR","IDX","IND","INDRX"};
#define NUMMOD	(sizeof(modtable)/sizeof(char *))

long encode(int opcode,int mode,int target,int index,int addr){
	long i;
    union instruction ii;

	ii.i1.first_part=0;
	ii.i1.address=(short int)addr;
	i=opcode<<2 | mode;
	i=i<<4	| target;
	i=i<<4	| index;
	i=i<<16	| ii.i2;
	return (i);
}

void decode(long i,int *opcode,int *mode,int *target,int *index,int *addr){
	union instruction ii; 
    short int s;
	
	ii.i2=i;
	*addr=		(int)ii.i1.address;	i=ii.i1.first_part;
	*index=		i & 0xF;	i=i>>4;
	*target=	i & 0xF;	i=i>>4;
	*mode=		i & 0x3;	i=i>>2;
	*opcode=	i & 0x3F;
}

void formatReg(int reg, char* str){
    if      (reg==15) strcpy(str,"SP");
    else if (reg==14) strcpy(str,"FP");
    else                 sprintf(str,"R%1d",reg);
}

void dispinst(FILE *fp,int opcode,int mode,int target,int index,int addr){
    char *reg = addr < 16 && opcode != MOVI ? "R" : "";
    char treg[4];   /* target register */
    char ireg[4];   /* index  register */
    
    formatReg(target,treg);
    formatReg(index,ireg);

	if (opcode<=MAX_NUM_PSEUDO)
		fprintf(fp,"%-5s %d",optable[opcode],addr);
	else
        switch (opcode){
        case MOV:
        case MOVM:
        case MOVI:
        case ISUB:
        case IADD:
        case IMUL:
        case IDIV:
        case LAND:
        case LOR :
        case XOR :
        case JLT:
        case JLE:
        case JGT:
        case JGE:
        case JEQ:
        case JNE:
        case JSP:
        case PUSH:
        case POP:
            /* Register + ea opcodes */
	        switch (mode){
		    case DIRECT:
		    fprintf (fp,"%-5s %s,%s%1d",optable[opcode],treg,reg,addr); break;
		    case INDIRECT:
		    fprintf (fp,"%-5s %s,@%1d",optable[opcode],treg,addr); break;
		    case INDEXED:
		    fprintf (fp,"%-5s %s,%1d[%s]",optable[opcode],treg,addr,ireg); break;
		    case INDIRIX:
		    fprintf (fp,"%-5s %s,@%1d[%s]",optable[opcode],treg,addr,ireg); break;
	        }
            break;
        case JMP:
        case IN:
        case OUT:
            /* address only opcodes */
	        switch (mode){
		    case DIRECT:
		    fprintf (fp,"%-5s %s%1d",optable[opcode],reg,addr); break;
		    case INDIRECT:
		    fprintf (fp,"%-5s @%1d",optable[opcode],addr); break;
		    case INDEXED:
		    fprintf (fp,"%-5s %1d[%s]",optable[opcode],addr,ireg); break;
		    case INDIRIX:
		    fprintf (fp,"%-5s @%1d[%s]",optable[opcode],addr,ireg); break;
	        }
            break;
        case RET:
            /* target only opcode */
            fprintf (fp,"%-5s %s",optable[opcode],treg); 
            break;
        case NOP:
        case SVC:
            /* no operand opcodes */
            fprintf (fp,"%-5s",optable[opcode]);
            break;
        }
}
