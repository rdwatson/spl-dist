/* st.c		v0.0
 * symbol table routines: FIXED LENGTH NAMES & SIMPLE TABLE ORGANIZATION
 */
#include <stdio.h>
#include <stdlib.h>
#include "st.h"

/* create a new symbol table, initially empty	*/
struct symtab *create_st(){
	int i;
	struct symtab *stp;

	stp=(struct symtab *)malloc(sizeof(struct symtab));
	for (i=0; i<MAXSTSIZE; i++)
		stp->ste[i].name[0]=0;
	return (stp);
}

/* remove all entries from table, free memory, and destroy it	*/
void destroy_st(struct symtab *st) {
	free(st);
}

/* enter name in table; return pointer to entry;
 * set present to true if name was already there else false
 */
void enter(struct symtab *st, char *name_str, struct st_entry **entry, int *present){
	int i,j;

	find (st,name_str,entry,present);
	if (!*present){
		/* ok to add new entry	*/
		for (i=0; i<MAXSTSIZE && st->ste[i].name[0] != '\0'; i++);
		if (i<MAXSTSIZE){
			for (j=0; j<MAXIDLEN; j++)
				if (( st->ste[i].name[j] = *name_str++ )==0)
					break;
			st->ste[i].name[j]=0; /* redundant if not truncated*/
			*entry = &st->ste[i];
		}
		else{
			fprintf(stderr,"fatal error: symbol table overflow\n");
			exit(1);
		}
	}
}

/* search for name in table;
 * if found return pointer to entry and set present to true
 * else set present to false
  */
void find(struct symtab *st, char *name_str, struct st_entry **entry, int *present){
	int i,j;
	char ss[MAXIDLEN+1];

	*present=0;
	if (st==NULL) return;
	/* make truncated copy of name_str	*/
	for (j=0; j<MAXIDLEN; j++)
		if ((ss[j] = name_str[j])=='\0') break;
	ss[j]='\0';
	/* search table and check for presence	*/
	for (i=0; i<MAXSTSIZE; i++)
		if (st->ste[i].name[0]=='\0')
			break;
		else if (strcmp(st->ste[i].name,ss)==0){
			*present=1;
			break;
		}
	if (*present)
		*entry = &st->ste[i];
	else
		*entry = NULL;
}

/* copy attributes pointed to by entry_attr to the corresponding
 * attributes in the symbol table entry
 */
void set_attr(struct st_entry *entry, struct attr *entry_attr){
	entry->id_attr = *entry_attr;
}

/* get attributes from the symbol table entry and copy to the
 * attribute fields in *entry_attr
 */
void get_attr(struct st_entry *entry, struct attr *entry_attr){
	*entry_attr = entry->id_attr;
}

/* list the contents of symbol table	
 */
void list_st(struct symtab *st){
	int i;
	struct st_entry *step;
	for (i=0; i<MAXSTSIZE; i++)
		if (st->ste[i].name[0]=='\0')
			break;
		else{
			step = &(st->ste[i]);
			printf("<%12s> <%1d> <%4d>",
				step->name,
				step->id_attr.id_class,
				step->id_attr.id_locn);
			if (step->id_attr.id_class == PROC_CLASS)	
				printf(" <%2d>",step->id_attr.num_locals);
			printf("\n");	
		}
}
