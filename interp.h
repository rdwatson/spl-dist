/*  debug parameter values       
 *  for use by interp(); 
 */
#define OFF     0
#define TRACE   1
#define SSTEP   2

void interp(long mem[], int startpc,int timelim,
            int memsize,int stklim, int debug);
